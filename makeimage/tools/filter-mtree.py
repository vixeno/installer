#!/usr/bin/env python3.4

import os.path
import sys
import re
import logging
import tempfile

logger = logging.getLogger('filter-mtree')


def parse_kv(text: str):
    return dict(re.findall(r'([^\s=]+)=(\S+)', text))


def escape(text: str) -> str:
    return re.sub(r'[^a-zA-Z0-9_\.~!@#$%^&*()[]{}"\':;<>,?|=\\-]',
                  lambda m: '\\' + format(ord(m.group(0)), 'o').rjust(3, '0'),
                  text)


class ImmutableList:
    def __init__(self, stream):
        self.paths = set()
        self.prefixes = []

        for line in stream.readlines():
            line = line.strip()
            if not line or line.startswith('#'):
                continue

            if line.endswith('*'):
                self.prefixes.append(line[:-2])
            else:
                self.paths.add(line)


    def immutable(self, path: str) -> bool:
        if path in self.paths:
            return True

        for prefix in self.prefixes:
            if path.startswith(prefix):
                return True

        return False


def filter_mtree(in_stream, out_stream, immutable: ImmutableList) -> None:
    cur = ''
    defaults = {}
    lines = iter(in_stream.readlines())
    for raw_line in lines:
        line = raw_line.strip()
        if not line or line.startswith('#'):
            continue

        while line.endswith('\\'):
            line = line.rstrip('\\')
            next_line = next(lines).strip()
            line += ' ' + next_line

        if line.startswith('/'):
            if line.startswith('/set'):
                defaults = parse_kv(line)
            else:
                logger.error('Unknown directive: %s', line)

            continue

        words = line.split(None, 1)
        segment = re.sub(r'\\([0-9]{3})',
                         lambda s: chr(int(s.group(1), 8)),
                         words[0])

        if segment == '..':
            cur = os.path.dirname(cur)
            out_stream.write('..\n')
            continue

        args = defaults.copy()
        args.update(parse_kv(words[1] if len(words) > 1 else ''))
        is_dir = 'type' in args and args['type'] == 'dir'
        if is_dir:
            if '/' not in segment:
                # Relative
                cur = os.path.join(cur, segment)
            else:
                # Full
                cur = segment.rstrip('/')
            filename = cur
        else:
            filename = os.path.join(cur, segment)

        abs_filename = filename.lstrip('.')
        if immutable.immutable(abs_filename):
            flags = args.get('flags', '').split(',')
            flags = [f for f in flags if f != 'none']
            if not 'schg' in flags:
                flags.append('schg')
                args['flags'] = ','.join(flags)

        args_str = ' '.join(['{}={}'.format(k, v) for k,v in args.items()])
        out_stream.write('{} {}\n'.format(escape(segment), args_str))


def usage(status: int) -> None:
    print('Usage: filter-mtree <manifest> <immutable>')
    sys.exit(status)


def main(args):
    try:
        mtree_path = args[1]
        immutable_path = args[2]
    except IndexError:
        usage(1)

    immutable = ImmutableList(open(immutable_path, 'r'))
    with tempfile.NamedTemporaryFile(mode='w',
                                     delete=False,
                                     prefix='mtree-') as out_file:
        try:
            filter_mtree(open(mtree_path, 'r'), out_file, immutable)
            os.rename(out_file.name, mtree_path)
        except Exception as err:
            os.unlink(out_file.name)
            raise err

if __name__ == '__main__':
    main(sys.argv)
