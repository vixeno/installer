#!/usr/bin/perl
use strict;
use warnings;

use Carp;
use Fcntl;
use Installer;

sub select_timezone {
    my ($tz) = @_;

    my %timezones = Installer::list_timezones();
    my @list = keys %timezones;
    @list = sort @list;

    my %countries = ();
    foreach my $zone(@list) {
        my ($country, $region) = split(/\//, $zone);
        if(!exists $countries{$country}) {
            $countries{$country} = [];
        }

        my $ref = $countries{$country};
        push(@$ref, $region);
    }

    while(1) {
        if(defined $tz) {
            print "Timezone [$tz] (? for help): ";
        } else {
            print "Timezone (? for help): ";
        }

        my $new_tz = <>;
        chomp $new_tz;
        if($new_tz eq '?') {
            print "Choose your country:\n\t";
            print join("\n\t", keys %countries);
            print "\nCountry: ";
            my $country = <>;
            chomp $country;

            if(!exists $countries{$country}) {
                print "Unknown country\n";
                next;
            }

            my $regions = $countries{$country};
            print "Choose your region:\n";
            print join("  ", @$regions);
            print "\nRegion: ";
            my $region = <>;
            chomp $region;

            $new_tz = "$country/$region";
            if(!exists $timezones{$new_tz}) {
                print "Unknown timezone\n";
                next;
            }

            return $new_tz;
        }

        if($new_tz eq '') {
            if(defined $tz) {
                $new_tz = $tz;
            } else {
                next;
            }
        }

        if(exists $timezones{$new_tz}) {
            return $new_tz;
        }

        print "Unknown timezone\n";
    }

    croak "Unreachable: Failed to get time zone";
}

sub main {
    my ($image_path) = @_;

    print "Downloading mirror information...\n";
    my %ftplist = Installer::get_ftplist('http://129.128.5.191/cgi-bin/ftplist.cgi');
    my $mirrors = $ftplist{'MIRRORS'};

    my @disks = Installer::list_disks();
    my $str_disks = join(', ', @disks);
    my $disk = '';

    until(grep {$_ eq $disk} @disks) {
        print "Disks: $str_disks\n";
        print 'Disk: ';
        $disk = <>;
        chomp $disk;
        if(!Installer::check_disk_size($disk)) {
            print "Disk too small\n";
            $disk = '';
        }
    }

    print 'Hostname: ';
    my $hostname = <>;
    chomp $hostname;

    print 'Username: ';
    my $username = <>;
    chomp $username;

    print 'Full Name: ';
    my $fullname = <>;
    chomp $fullname;

    my $timezone = select_timezone($ftplist{'TZ'});

    print "Installing...\n";
    Installer::umount_disk();
    my ($duid, @fstab) = Installer::setup_disk($disk);
    Installer::mount_disk $duid;
    Installer::extract_image($image_path);
    Installer::configure($duid, @fstab);
    Installer::configure_hostname($hostname);
    Installer::configure_timezone($timezone);
    Installer::configure_mirrors(@$mirrors);
    Installer::create_user($username, $fullname);

    Installer::umount_disk();
    return;
}

sub lock_installer {
    IO::File->new("/tmp/installer.lock", O_CREAT|O_EXCL) or die "Failed to lock";
    return;
}

sub unlock_installer {
    unlink("/tmp/installer.lock");
    return;
}

sub usage {
    die("Usage: install.pl <imagepath>\n");
}

local $SIG{INT} = sub { unlock_installer(); die "Caught sigint"; };
local $SIG{TERM} = sub { unlock_installer(); die "Caught sigterm"; };

lock_installer();
eval { main(shift or usage()); }; warn $@ if $@;
unlock_installer();
